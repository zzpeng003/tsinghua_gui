QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    connectiondialog.cpp \
    data_source.cpp \
    main.cpp \
    mainwindow.cpp \
    mainwindow1.cpp \
    sim_conf.cpp

HEADERS += \
    common.h \
    connection.h \
    connectiondialog.h \
    data_source.h \
    mainwindow.h \
    mainwindow1.h \
    sim_conf.h \

FORMS += \
    connectiondialog.ui \
    data_source.ui \
    mainwindow.ui \
    mainwindow1.ui \
    sim_conf.ui \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
