#ifndef SIM_CONF_H
#define SIM_CONF_H

#include <QDialog>

namespace Ui {
class sim_conf;
}

class sim_conf : public QDialog
{
    Q_OBJECT

public:
    explicit sim_conf(QWidget *parent = nullptr);
    ~sim_conf();

private:
    Ui::sim_conf *ui;
};

#endif // SIM_CONF_H
