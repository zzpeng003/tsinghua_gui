#ifndef CONNECTION_H
#define CONNECTION_H

#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

#include "common.h"

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    qDebug() << "QSQLITE driver valid?" << db.isValid();
#ifdef local_database
    db.setDatabaseName("DecodeInfo.db");
#endif

#ifdef memory_database
    db.setDatabaseName(":memory:");
#endif

    if (!db.open()) {
        QMessageBox::critical(nullptr, QObject::tr("Cannot open database"),
            QObject::tr("Unable to establish a database connection.\n"
                        "This example needs SQLite support. Please read "
                        "the Qt SQL driver documentation for information how "
                        "to build it.\n\n"
                        "Click Cancel to exit."), QMessageBox::Cancel);
        return false;
    }

#ifdef memory_database
    QSqlQuery query;
    /*method two
    QString create_table = "create table student (id int primary key,name varchar(20))";
    query.prepare(create_table);
    query.exec();
    QString insert_target1 = "insert into student values(1,'liming')";
    query.prepare(insert_target1);
    query.exec();
    QString insert_target2 = "insert into student values(2,'zhang')";
    query.prepare(insert_target2);
    query.exec();
    QString insert_target3 = "insert into student values(3,'zhu')";
    query.prepare(insert_target3);
    query.exec();*/

    /*methed one*/
    query.exec("create table student (id int primary key,name varchar(20),genda varchar(10))");
    query.exec("insert into student values(0,'liming','male')");
    query.exec("insert into student values(1,'zhang','female')");
    query.exec("insert into student values(2,'shang','unknown')");

#endif

#ifdef local_database
    QSqlQuery query;
    QString create_table = "create table DecodeInfo1(targetname varchar(20), TMSI int primary key, RAC varchar(20))";
    /*if(query.prepare(create_table))
    {
        qDebug()<<"table created success";
        return true;
    }
    else
    {
        qDebug()<<"fail to create table";
        return false;
    }*/
    query.prepare(create_table);
    query.exec();
    QString insert_operation = "insert into DecodeInfo1 values('target1', 5, 'yes')";
#ifdef easy_debug
    if(query.prepare(insert_operation))
    {
        qDebug()<<"target inserted success";
        return true;
    }
    else
    {
        qDebug()<<"fail to insert target";
        return false;
    }
#endif
    query.prepare(insert_operation);
    query.exec();
#endif
    return true;
}

#endif // CONNECTION_H
