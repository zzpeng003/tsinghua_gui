#include "connection.h"
#include "targetlist.h"
#include "ui_targetlist.h"
#include "mytableview.h"

TargetList::TargetList(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TargetList)
{
    ui->setupUi(this);




    /************与其它界面建立连接*******/
    connect(ui->locallist, &QAction::triggered,this, &TargetList::OpenLocalInfo);
    connect(ui->boardlist, &QAction::triggered,this, &TargetList::OpenBoardInfo);
    connect(ui->targetlist, &QAction::triggered,this, &TargetList::OpenLogin);  //打开Login信号与槽  //5
    connect(ui->connection, &QAction::triggered,this, &TargetList::ConnectToDatabase);

    /**********************************/
}

TargetList::~TargetList()
{
    delete ui;
}

void TargetList::OpenLogin()    //4
{
    AddTargetDialog *quicklinklogin = new AddTargetDialog();  //定义了一个LogIn对象
    emit QuickLinkLogin();
    quicklinklogin->show();
}

void TargetList::OpenBoardInfo()
{
    BoardInfoDialog *quicklinkboardinfo = new BoardInfoDialog();  //定义一个BoardInfo对象
    emit QuickLinkBoardInfo();
    quicklinkboardinfo->show();
}

void TargetList::OpenLocalInfo()
{
    LocalInfoDialog *quicklinklocalinfo = new LocalInfoDialog();
    emit QuickLinkLocalInfo();
    quicklinklocalinfo->show();
}

void TargetList::ConnectToDatabase()
{
    //close();
    addSqliteConnection();
    creatDB();
    bindModelwithDatabase();
    setQTableViewHeader();
}

void TargetList::addSqliteConnection()
{
    const QString strDatabasePath = QApplication::applicationDirPath()+ "/MyDataBase.db";
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(strDatabasePath);
    if (!db.open())
    {
        QMessageBox::critical(nullptr, QObject::tr("Cannot open database"),
            QObject::tr("Unable to establish a database connection.\n"
                        "This example needs SQLite support. Please read "
                        "the Qt SQL driver documentation for information how "
                        "to build it.\n\n"
                        "Click Cancel to exit."), QMessageBox::Cancel);
        return ;
    }
    else
    {
        qDebug()<<"Sqlite database created success";
    }
}

void TargetList::creatDB()
{
    QSqlQuery query;
    query.exec("create table factory (id int primary key, manufactory varchar(40), address varchar(40))");
    query.exec(QObject::tr("insert into factory values(1, '一汽大众', '长春')"));
    query.exec(QObject::tr("insert into factory values(2, '二汽神龙', '武汉')"));
    query.exec(QObject::tr("insert into factory values(3, '上海大众', '上海')"));

    query.exec("create table cars (carid int primary key, name varchar(50), factoryid int, year int, foreign key(factoryid) references factory)");
    query.exec(QObject::tr("insert into cars values(1,'奥迪A6',1,2005)"));
    query.exec(QObject::tr("insert into cars values(2, '捷达', 1, 1993)"));
    query.exec(QObject::tr("insert into cars values(3, '宝来', 1, 2000)"));
    query.exec(QObject::tr("insert into cars values(4, '毕加索',2, 1999)"));
    query.exec(QObject::tr("insert into cars values(5, '富康', 2, 2004)"));
    query.exec(QObject::tr("insert into cars values(6, '标致307',2, 2001)"));
    query.exec(QObject::tr("insert into cars values(7, '桑塔纳',3, 1995)"));
    query.exec(QObject::tr("insert into cars values(8, '帕萨特',3, 2000)"));
}

void TargetList::bindModelwithDatabase()
{
    m_pModel = new QSqlTableModel();
    m_pModel->setTable("student");
    m_pModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    if(!m_pModel->select())
    {
        qDebug()<<"DataBaseMidware::bindModelAndDataBase："<<"no data"<<endl;
    }
}

void TargetList::setQTableViewHeader()
{
    m_pModel->setHeaderData(0, Qt::Horizontal, "");
    m_pModel->setHeaderData(1, Qt::Horizontal, "ID");
    m_pModel->setHeaderData(2, Qt::Horizontal, "Manufactor");
    m_pModel->setHeaderData(3, Qt::Horizontal, "Address");
}
