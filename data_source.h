#ifndef DATA_SOURCE_H
#define DATA_SOURCE_H

#include <QDialog>

namespace Ui {
class data_source;
}

class data_source : public QDialog
{
    Q_OBJECT

public:
    explicit data_source(QWidget *parent = nullptr);
    ~data_source();

private:
    Ui::data_source *ui;
};

#endif // DATA_SOURCE_H
